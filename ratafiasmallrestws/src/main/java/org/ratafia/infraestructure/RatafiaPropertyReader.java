package org.ratafia.infraestructure;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.ratafia.model.RatafiaConstants;

public class RatafiaPropertyReader implements RatafiaConstants {


	private static final String propFilename = "config.properties";
	private String prop = "";
	private Properties propFile = new Properties();
	
	public Properties getConfigProperty() throws IOException{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFilename);
		if (inputStream!=null){
			propFile.load(inputStream);
		} else {
			throw new FileNotFoundException("Property file "+ propFilename+" not found in classpath");
		}
		return propFile;
	}
	
	public String getProperty(String property) throws IOException{
		return getConfigProperty().getProperty(property);
	}
}
