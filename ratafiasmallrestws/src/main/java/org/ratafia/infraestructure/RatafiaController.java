package org.ratafia.infraestructure;

import javax.ws.rs.core.Response;

public interface RatafiaController {
    Response add(Integer value);
    
    Integer get();
    
}
