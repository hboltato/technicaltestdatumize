package org.ratafia.client;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.ratafia.infraestructure.RatafiaPropertyReader;
import org.ratafia.model.RatafiaConstants;

public class RatafiaClient {
	
	private Client client = ClientBuilder.newClient( new ClientConfig() );
	private WebTarget webTarget;
	private final String SYNC = "SYNC";
	private RatafiaPropertyReader pr = new RatafiaPropertyReader();
	
	
	public RatafiaClient() throws IOException {
		String port = pr.getProperty(RatafiaConstants.PORT);
		String resource = pr.getProperty(RatafiaConstants.RESOURCE);
		webTarget = client.target("http://localhost:"+port+"/"+resource);
	}

	public Integer getNumber() throws IOException, InterruptedException, ExecutionException{
		Integer num;
		if (pr.getProperty(RatafiaConstants.SYNC_OR_ASYNC).equals(SYNC)){
			Response response = webTarget.path("get").request(MediaType.APPLICATION_XML).get();
			num = response.readEntity(Integer.class);
		} else {
			Future<Integer>responses =  webTarget.path("get").request(MediaType.APPLICATION_XML).async().get(Integer.class);
			num = responses.get();
			System.out.println(num);
		}
		return num;
	}
	
	public void addNumber(Integer number) throws IOException{
		
		if (pr.getProperty(RatafiaConstants.SYNC_OR_ASYNC).equals(SYNC)){
			webTarget.request(MediaType.APPLICATION_XML).post(Entity.entity(number, MediaType.APPLICATION_XML));
		} else {
			webTarget.path("get").request(MediaType.APPLICATION_XML).async().post(Entity.entity(number, MediaType.APPLICATION_XML));
		}
		
	}
}
