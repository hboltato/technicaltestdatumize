package org.ratafia.api;

import org.apache.http.HttpStatus;
import org.ratafia.infraestructure.RatafiaController;
import org.ratafia.model.RatafiaCounter;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ratafia")
public class RatafiaRESTController implements RatafiaController {

    @Inject RatafiaCounter ratafiaCounter;

    @Override
    @GET
    @Consumes({MediaType.APPLICATION_XML})
    @Path("/add")
    public Response add(Integer value) {
        ratafiaCounter.add(value);
        return Response.status(HttpStatus.SC_OK).build();
    }
    
    
    @Override
    @GET 
    @Produces({MediaType.APPLICATION_XML})
    @Path("/getCounter")
    public Integer get() {
    	return  ratafiaCounter.get();
    }
}
