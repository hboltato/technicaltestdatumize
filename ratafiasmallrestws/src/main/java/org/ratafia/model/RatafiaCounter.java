package org.ratafia.model;

public class RatafiaCounter {

    private static RatafiaCounter THE_ONE;
    private Integer accumulator;
    
    private static Object mutex = new Object();

    private RatafiaCounter(){
    	THE_ONE = new RatafiaCounter();
        accumulator = 0;
    }

    public static RatafiaCounter getInstance(){
        return THE_ONE;
    }

    public void add(Integer quantity){
        accumulator += quantity;
    }
    
    public Integer get(){
    	return accumulator;
    }
    
    
    public static final RatafiaCounter getSigleton(){
    	RatafiaCounter saved = THE_ONE;
		if (saved == null){
			synchronized ( mutex) {
				saved = THE_ONE;
				if (saved == null){
					saved = THE_ONE = new RatafiaCounter();
				}
			}
			THE_ONE = new RatafiaCounter();
		}
		return THE_ONE;
	}

}
