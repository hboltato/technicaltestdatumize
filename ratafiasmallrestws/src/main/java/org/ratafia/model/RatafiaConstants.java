package org.ratafia.model;

public interface RatafiaConstants {

	 static final String SYNC_OR_ASYNC = "SYNC_or_ASYNC";
	 static final String PORT = "8081";
	 static final String RESOURCE = "myresource";
}