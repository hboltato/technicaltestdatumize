package org.ratafia.feature;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.io.IOException;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.ratafia.infraestructure.RatafiaPropertyReader;
import org.ratafia.model.RatafiaConstants;

import io.restassured.RestAssured;

public class RatafiaRESTFeature {

    @BeforeAll
    public static void init() throws NumberFormatException, IOException{
    	RatafiaPropertyReader pr = new RatafiaPropertyReader();
    	String resource = pr.getProperty(RatafiaConstants.RESOURCE);
        RestAssured.baseURI = "http://localhost/"+resource;
        RestAssured.port = new Integer(pr.getProperty(RatafiaConstants.PORT)).intValue();
    }

    @Test
    public void add_integer_returns_200_code(){
        given().body("{ value: 3 }").when().post("/add").then().assertThat().statusCode(is(equalTo(HttpStatus.SC_OK)));
    }
}
