package org.ratafia.rest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.ratafia.api.RatafiaRESTController;
import org.ratafia.infraestructure.RatafiaController;
import org.ratafia.model.RatafiaCounter;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class RatafiaRESTControllerShould {

    private RatafiaController ratafiaController = new RatafiaRESTController();
    @Captor
    ArgumentCaptor<Integer> argumentCaptor;
    @Mock
    private RatafiaCounter ratafiaCounter;

    @Test
    public void add_value_when_add_integer(){
        ratafiaController.add(4);
        Mockito.verify(ratafiaCounter).add(argumentCaptor.capture());
        assertEquals(Integer.valueOf(4), argumentCaptor.getValue());
    }
}
